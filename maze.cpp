/*
Author:  Ryan Hartman
Course:  COP3530-Data Structures
Semester:  Fall 2011
Project:  Assignment #3
Due Date:  10-31-2011 @11:30PM EST
File Name: "maze.cpp"

Description:
This source file contains code which finds a route through an 8x8 maze.  This program uses the stack ADT to store all moves
made throughout the maze, and during the maze traversal, compares the current position to the previous moves in the maze.
This allows the program to determine if it has backed into a corner, if it's moving backwards, or if it can make a move.
Once it has been determined whether or not the maze can be solved, an output file, named by the user, is created and will either
contain coordinates to exit the maze successfully or will tell the user there is no exit.
*/

//preprocessor directives
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

//CAPACITY is the square dimension of the maze.  in this case, 8... for 8x8 maze
const int CAPACITY = 8;

//definition of 'coordinates'
class coordinates
{
public:
	//actual maze coordinates
	int x, y;
	//label is only really used for error checking
	string label;
};


//definition of node
class node
{
public:
	//contains coordinates and pointer to next object.  public so it can be accessed anywhere
	coordinates data;
	node *next;
};

//definition of stack
class stack
{
public:
	//member function prototoypes:
	//default constructor
	stack();
	//destructor
	~stack();
	//push adds coordinates to top of stack
	void push(coordinates);
	//pop removes coordinates from top of stack
	void pop();
	//print writes contents of stack in reverse order to a file
	void print(ofstream &out);
	//setCoords takes coordinates and writes whatever is on the top of the stack to them
	void setCoords(coordinates &coords);
	//determines if a stack is empty
	bool isEmpty() {return (top == 0);}
	//determines if moving in a specific direction will make a move that already exists in the stack
	bool caseComparison(int dir, coordinates &coords);
private:
	//pointer to first element in stack
	node *top;
};

//default constructor
stack::stack()
{
	top = 0;
}

//destructor
stack::~stack()
{
	//if empty, do nothing
	if(top == 0)
	{
		return;
	}
	node *p, *q;
	p = top;
	//delete all nodes until none exist
	while(p != 0)
	{
		q = p->next;
		delete p;
		p = q;
	}
	//top now points to nothing
	top = 0;
}

//push adds coordinates to top of stack
void stack::push(coordinates orig)
{
	node *p;
	p = top;
	top = new node;
	//copy data from formal params to new element
	top->data = orig;
	//top's next pointer now points to where top pointed before execution
	top->next = p;
}

//pop removes coordinates from top of stack
void stack::pop()
{
	//if empty, exit
	if(top == 0)
	{
		return;
	}

	node *p;
	//p points to second element
	p = top->next;
	//delete first element
	delete top;
	//top points to second element
	top = p;
}

//print writes contents of stack in reverse order to a file
void stack::print(ofstream &out)
{
	node *p, *q;
	p = top;
	q = top;
	//if empty, bail out
	if(p == 0)
	{
		return;
	}
	int length = 0;
	//while q has not traversed entirely through the stack
	//this loop finds out how many nodes exist in a stack
	while(q != 0)
	{
		q = q->next;
		length++;
	}

	int j = 0;

	//i decrements to 0
	for(int i = length; i != 0; i--)
	{
		//this segment sets p to the node at position j
		while(j != i - 1)
		{
			p = p->next;
			j++;
		}
		//output coordinates
		out<<'('<<p->data.y<<','<<p->data.x<<')'<<endl;
		//reset for nested while loop
		j = 0;
		p = top;
	}
}

//setCoords takes coordinates and writes whatever is on the top of the stack to them
void stack::setCoords(coordinates &coords)
{
	//if empty, bail out
	if(top == 0)
	{
		return;
	}
	//coords in formal parameters are set to those of top element in stack
	coords.x = top->data.x;
	coords.y = top->data.y;
	return;
}

//determines if moving in a specific direction will make a move that already exists in the stack
//returns false if the move has already been made
bool stack::caseComparison(int dir, coordinates &coords)
{
	node *p;
	p = top;
	//if empty, bail out
	if(p == 0)
	{
		return true;
	}
	//if direction is west
	if(dir == 0)
	{
		//traverse through stack
		while(p != 0)
		{
			//if the coordinates of the move you're about to make match ones anywhere in the stack, return false
			if(p->data.x == (coords.x - 1) && p->data.y == coords.y)
			{
				return false;
			}
			p = p->next;
		}
		return true;
	}
	//if direction is north
	else if(dir == 1)
	{
		//traverse through stack
		while(p != 0)
		{
			//if the coordinates of the move you're about to make match ones anywhere in the stack, return false
			if(p->data.x == coords.x && p->data.y == (coords.y - 1))
			{
				return false;
			}
			p = p->next;
		}
		return true;
	}
	//if direction is east
	else if(dir == 2)
	{
		//traverse through stack
		while(p != 0)
		{
			//if the coordinates of the move you're about to make match ones anywhere in the stack, return false
			if(p->data.x == (coords.x + 1) && p->data.y == coords.y)
			{
				return false;
			}
			p = p->next;
		}
		return true;
	}
	//if direction is south
	else if(dir == 3)
	{
		//traverse through stack
		while(p != 0)
		{
			//if the coordinates of the move you're about to make match ones anywhere in the stack, return false
			if(p->data.x == coords.x && p->data.y == (coords.y + 1))
			{
				return false;
			}
			p = p->next;
		}
		return true;
	}
	else
	{
		return true;
	}
}

//initCoords initializes a set of coordinates based on a position in a maze
//the user enters the character they're looking for and initCoords sets the coordinates of the first instance
void initCoords(const string maze[], coordinates &mod, string name, char query)
{
	//traverse though maze
	for(int i = 0; i < CAPACITY; i++)
	{
		for(int j = 0; j < CAPACITY; j++)
		{
			//if maze at position y, x (cartesian coordinates in reverse order)
			if(maze[i][j] == query)
			{
				//modify coordinates and return
				mod.x = j;
				mod.y = i;
				mod.label = name;
				return;
			}
		}
	}
	//if not found, then they do not exist
	mod.label = "DNE";
}

//makeMove checks conditions to determine if a move can be made in a specific direction
//if possible, it makes a move and writes the coordinates to the stack
bool makeMove(char direction, coordinates &pos, stack &history, string yourMaze[])
{
	//If calling function wants to move west
	if(direction == 'W')
	{
		//verify boundaries are ok
		if(pos.x == 0 || pos.x > CAPACITY - 1)
		{
			return false;
		}
		//verify you're not going to hit a wall
		if(yourMaze[pos.y][pos.x - 1] == '8')
		{
			return false;
		}
		//verify the move has not yet been made
		if(history.caseComparison(0, pos) == false)
		{
			return false;
		}
		//move
		pos.x -= 1;
		//write to stack
		history.push(pos);
		//returns true to indicate successful move
		return true;
	}
	//if calling function wants to move west
	else if(direction == 'N')
	{
		//check boundaries
		if(pos.y >= (CAPACITY) || pos.y == 0)
		{
			return false;
		}
		//verify not hitting wall
		if(yourMaze[pos.y - 1][pos.x] == '8')
		{
			return false;
		}
		//verify move has not yet been made
		if(history.caseComparison(1, pos) == false)
		{
			return false;
		}
		//move
		pos.y -= 1;
		//write to stack
		history.push(pos);
		//move successful
		return true;
	}
	//if calling function wants to move east
	else if(direction == 'E')
	{
		//check boundaries
		if(pos.x == (CAPACITY - 1) || pos.x < 0)
		{
			return false;
		}
		//make sure you're not hitting a wall
		if(yourMaze[pos.y][pos.x + 1] == '8')
		{
			return false;
		}
		//verify the move has not yet been made
		if(history.caseComparison(2, pos) == false)
		{
			return false;
		}
		//move
		pos.x += 1;
		//write to stack
		history.push(pos);
		//successful move
		return true;
	}
	//if calling function wants to move south
	else if(direction == 'S')
	{
		//check boundaries
		if(pos.y < 0 || pos.y == (CAPACITY - 1))
		{
			return false;
		}
		//make sure not hitting a wall
		if(yourMaze[pos.y + 1][pos.x] == '8')
		{
			return false;
		}
		//verify move has not been made yet
		if(history.caseComparison(3, pos) == false)
		{
			return false;
		}
		//move
		pos.y += 1;
		//write to stack
		history.push(pos);
		//successful move
		return true;
	}
	else
	{
		return false;
	}
}

//driver
int main()
{
	//stack stores move history
	stack moves;
	//3 sets of coordinates are created
	coordinates start, finish, retainer;
	//input and output file streams
	ifstream in;
	ofstream out;
	//filename
	string filename;
	//array of strings contains maze
	string maze[CAPACITY];
	//character array contains directions for makeMoves() function
	char dirs[4] = {'W', 'N', 'E', 'S'};
	//run the following at least one time
	do
	{
		//enter file name
		cout<<"Please enter the filename of your maze: ";
		cin>>filename;
		//try to open
		in.open(filename.c_str());
		//if it did not open, it doesn't exist
		if(!in)
		{
			cout<<"File not found."<<endl;
		}
	}
	//try again
	while(!in);

	//write maze contents from file to array of strings
	int j = 0;
	while(!in.eof())
	{
		in>>maze[j];
		j++;
	}
	//close input file when done
	in.close();

	//initialize coordinates for start finish, and start again in coordinates 'retainer'
	initCoords(maze, start, "start", '1');
	initCoords(maze, finish, "finish", '9');
	initCoords(maze, retainer, "retainer", '1');

	//if either endpoint is missing, quit
	if(start.label == "DNE" || finish.label == "DNE")
	{
		cout<<"Your maze is missing an endpoint."<<endl;
		return 0;
	}

	//this portion of the program finds the solutions
	bool moveMade;
	int x;
	//do this at least once
	do
	{
		//reset moveMade and x
		moveMade = false;
		x = 0;
		while(moveMade == false && x < 4)
		{
			//try to move in dirs[x] direction.  moveMade stores result of attempt
			moveMade = makeMove(dirs[x], start, moves, maze);
			//increment x (new direction
			x++;
			//if you have tried to move in all 4 directions without success...
			if(x == 4 && moveMade == false)
			{
				//you've hit a dead end.
				//set the current maze location to 8, so you don't go back
				maze[start.y][start.x] = '8';
				//pull the bad move from the stack
				moves.pop();
				//update coordinates to previous location
				moves.setCoords(start);
			}
		}
	}
	//loop iterates until the program finishes or determines there is no exit
	while(!moves.isEmpty() && maze[start.y][start.x] != '9');
	
	//request output filename from user
	cout<<"Please enter output filename: ";
	cin>>filename;
	out.open(filename.c_str());
	//if a way out was found
	if(maze[start.y][start.x] == '9')
	{
		//write the following to the output file
		out<<"A solution has been found:"<<endl;
		out<<"(y,x):"<<endl;
		//retainer contains first coordinates in maze which are lost during processing
		out<<'('<<retainer.y<<','<<retainer.x<<')'<<endl;
		//print takes reference to output file stream
		moves.print(out);
	}
	//if there is no solution, tell the user about the unfortunate news
	if(moves.isEmpty())
	{
		out<<"There is no way out of the maze."<<endl;
	}

	//close the output file.
	out.close();

	//turn the lights off when you leave
	return 0;
}